# jfxs / slack-notify

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Pipeline Status](https://gitlab.com/fxs/docker-slack-notify/badges/master/pipeline.svg)](https://gitlab.com/fxs/docker-slack-notify/pipelines)
[![Robot Framework Tests](https://fxs.gitlab.io/docker-slack-notify/all_tests.svg)](https://fxs.gitlab.io/docker-slack-notify/report.html)
[![Image size](https://fxs.gitlab.io/docker-slack-notify/docker.svg)](https://hub.docker.com/r/jfxs/slack-notify)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

A lightweight Docker image to easily publish Slack message in a gitlab-ci pipeline to build Docker image.

This image is automatically updated with the [Automated Continuous Delivery of Docker images](https://medium.com/@fx_s/automated-continuous-delivery-of-docker-images-b4bfa0d09f95) pattern.

## Getting Started

### Prerequisities

In order to run this container you'll need Docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

Include template at the beginning of .gitlab-ci.yml file:

```shell
include:
  - remote: 'https://gitlab.com/fxs/docker-slack-notify/raw/master/.slack-notify-template.yml'
```

Set CI/CD variable:

* T_SLACK_WEBHOOK, example: Z3FBDHDE/DFEGRLHF5HE/FuIhzlJl3lJLRHlj (must be masked)
* T_DOCKERHUB_IMAGE_REPOSITORY, example: jfxs/slack-notify

## Built with

Docker latest tag contains:

See versions on [Dockerhub](https://hub.docker.com/r/jfxs/slack-notify)

Versions of installed software are listed in /etc/VERSIONS file of the Docker image. Example to see them for a specific tag:

```shell
docker run -t jfxs/slack-notify:1.0.0-1 cat /etc/VERSIONS
```

## Tests

Tests are runned with [Robot Framework](http://robotframework.org/).
Last test report is available [here](https://fxs.gitlab.io/docker-slack-notify/report.html).

## Find Us

* [Dockerhub](https://hub.docker.com/r/jfxs/slack-notify)
* [Gitlab](https://gitlab.com/fxs/docker-slack-notify)

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/docker-slack-notify/blob/master/LICENSE) file for details.
