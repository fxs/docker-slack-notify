#!/bin/sh
#
# slack-notify.sh is a shell program to publish a message in a slack channel.
#
# Copyright (c) 2019 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# slack-notify.sh uses the Open Source/Free Software "curl" by Daniel Stenberg
# distributed under the MIT/X derivate license:
# https://curl.haxx.se/docs/copyright.html

VERSION=__VERSION__

SLACK_ENDPOINT="https://hooks.slack.com/services"
SLACK_DOC="https://api.slack.com/messaging/webhooks"

SLACK_WEBHOOK=""
MESSAGE=""
TEXT_MESSAGE=""
MANDATORY_ARGUMENT=1

usage() {
    echo "This script publishes a message in a slack channel."
    echo ""
    echo "Usage: slack-notify.sh -w <webhook> -m <message> [-p <plain-text version message>]"
    echo "Options:"
    echo "  -w Slack web hook for the channel"
    echo "  -m Message to display in Markdown"
    echo "  -p Message to display in plain-text for notification (Optional)"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Slack webhook documentation: $SLACK_DOC"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
    echo "slack-notify.sh uses the Open Source/Free Software \"curl\" by Daniel Stenberg"
    echo "distributed under the MIT/X derivate license:"
    echo "https://curl.haxx.se/docs/copyright.html"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts hp:m:vw: option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    p)
        TEXT_MESSAGE="${OPTARG}"
        ;;
    v)
        version
        exit 0
        ;;
    w)
        SLACK_WEBHOOK="${OPTARG}"
        ;;
    m)
        MESSAGE="${OPTARG}"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "w" "$SLACK_WEBHOOK"
test_argument "m" "$MESSAGE"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

if ! [ -x "$(command -v curl)" ]; then
    echo "curl command not found" >&2
    exit 1
fi

if [ -z "$TEXT_MESSAGE" ]; then
    TEXT_MESSAGE="$MESSAGE"
fi

slack_response=$(curl -w "%{http_code}" -s -H "Content-Type: application/json" -X POST -d "{\"text\": \"${TEXT_MESSAGE}\", \"blocks\": [{\"type\": \"section\", \"text\": {\"type\": \"mrkdwn\", \"text\": \"${MESSAGE}\"}}]}" "${SLACK_ENDPOINT}/${SLACK_WEBHOOK}")
http_code=$(echo "$slack_response" | tr -d '\n' | tail -c 3)
if [ "${http_code}" != "200" ]; then
    printf "Unable to publish message in Slack, response code: %s\\n" "${http_code}" >&2
    exit 1
fi
