** Settings ***
Documentation     slack-notify.sh tests
...
...               A test suite to validate slack-notify shell script.

Library           OperatingSystem
Library           Process
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   slack-notify.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -w
${WEB_HOOK}   XXXX/YYYY/ZZZZ
${MESSAGE}   Message from slack-notify.sh RF tests
${TEXT_MESSAGE}   Plan-text message from slack-notify.sh RF tests

*** Test Cases ***
with no option
    [Tags]    slack-notify    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -w argument is mandatory
    And Should Contain    ${result.stderr}    -m argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    slack-notify    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-m] option missing
    [Tags]    slack-notify    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -w   ${WEB_HOOK}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -m argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-m] option with missing argument
    [Tags]    slack-notify    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -w   ${WEB_HOOK}   -m
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option with missing argument
    [Tags]    slack-notify    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -w   ${WEB_HOOK}   -m   ${MESSAGE}   -p
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-w] option missing
    [Tags]    slack-notify    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -m   ${MESSAGE}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -w argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-w] option with missing argument
    [Tags]    slack-notify    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -m   ${MESSAGE}   -w
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-w] option argument with wrong web hook
    [Tags]    slack-notify    curl
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -w   XXXX/YYYY/ZZZZ   -m   ${MESSAGE}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to publish message in Slack

[-v] option
    [Tags]    slack-notify    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    slack-notify    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    slack-notify    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

Curl not installed
    [Tags]    slack-notify    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -w   ${WEB_HOOK}   -m   ${MESSAGE}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    curl command not found

Successful usage without [-p] option
    [Tags]    slack-notify    curl
    Log   ${WEB_HOOK}
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -w   ${WEB_HOOK}   -m   ${MESSAGE}
    Then Should Be Equal As Integers    ${result.rc}    0

Successful usage with [-p] option
    [Tags]    slack-notify    curl
    Log   ${WEB_HOOK}
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -w   ${WEB_HOOK}   -m   ${MESSAGE}   -p   ${TEXT_MESSAGE}
    Then Should Be Equal As Integers    ${result.rc}    0
